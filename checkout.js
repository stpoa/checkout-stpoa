const products = require('./products')

// Checkout class
function Checkout () {
  this.productCounts = {} // { productId: productCount, }
  this.quantityDiscounts = [] // [{ id, discount }, ]
  this.abDiscounts = [] // [{ ids, discount }]
  this.percentDiscount = 0
  // Add new product to checkout
  this.add = function (product, quantity = 1) {
    // Zero if not added previouasly
    if (!this.productCounts[product.id]) {
      this.productCounts[product.id] = 0
    }
    // Increment product counts
    this.productCounts[product.id] += quantity
  }
  // Sum all products with all discounts
  this.sum = function () {
    return (sumWithoutDiscounts() - quantityDiscount() - abDiscount() -
      percentDiscount())
  }
  // Generate receipt string
  this.receipt = function () {
    const productsReceipt = Object.entries(this.productCounts)
    .reduce((str, arr) => {
      const id = arr[0]
      const count = arr[1]
      const price = products[id].price
      return `${str}${id}\tx${count}\t${price} PLN\n`
    }, '')
    const quantitydiscountsReceipt = this.quantityDiscounts
    .reduce((str, obj) => {
      const id = Object.keys(obj)[0]
      const amount = Object.values(obj)[0]
      return `${str}${id}\t\t-${amount} PLN\n`
    }, '')
    const abDiscountsReceipt = this.abDiscounts
    .reduce((str, obj) => {
      const ids = Object.keys(obj)[0].split('|').join('+')
      const amount = Object.values(obj)[0]
      return str + ids + '\t\t-' + amount + ' PLN\n'
    }, '')
    const percentDiscount = this.percentDiscount
      ? `-${this.percentDiscount}\t\t\tPLN\n`
      : ''

    return (productsReceipt + 'Discounts \n' + quantitydiscountsReceipt +
      abDiscountsReceipt + percentDiscount)
  }
  // Sum all products without discounts
  const sumWithoutDiscounts = () => Object.keys(this.productCounts)
  .reduce((acc, key) => acc + products[key].price * this.productCounts[key], 0)

  // Calculate percent discount
  const percentDiscount = () => {
    const sum = sumWithoutDiscounts()
    const percent = 10
    const threshold = 600

    this.percentDiscount = sum >= threshold ? sum * (percent / 100) : 0
    return this.percentDiscount
  }

  // Calculate quantity discount
  const quantityDiscount = () => {
    this.quantityDiscounts = Object.keys(this.productCounts).map(key => {
      const product = products[key]
      const { price, quantityDiscounts } = product
      const count = this.productCounts[key]

      // If product doesn't have a discount
      if (!product.quantityDiscounts) return ({ [product.id]: 0 })

      // If smallest discount doesn't apply
      if (Number(Object.keys(quantityDiscounts)[0]) > count) {
        return ({ [key]: 0 })
      }

      // Takes biggest possible discount
      let maxCount = 0
      Object.keys(quantityDiscounts).reverse().some((k, i) => {
        if (Number(k) <= count) {
          maxCount = Number(k)
          return true
        }
      })

      // Calculate discount amount
      const discountCount = maxCount
      const discountPrice = quantityDiscounts[maxCount]
      const result = price * count - Math.floor(count / discountCount) *
        discountPrice - (count % discountCount) * price
      // Return discount amount with key for each product
      return ({ [key]: result })
    }).filter(discount => !!Object.values(discount)[0]) // filter 0-discounts

    // Calculate total quantity discount
    const totalDiscount = this.quantityDiscounts.reduce((sum, discount) =>
      sum + Object.values(discount)[0], 0)

    return totalDiscount
  }

  // Calculate abdiscount
  const abDiscount = () => {
    this.abDiscounts = Object.keys(this.productCounts).map(key => {
      const product = products[key]

      // If product doesn't have a discount
      if (!product.abDiscounts) return {[product.id]: 0}

      const { abDiscounts } = product
      const abDiscountId = Object.keys(abDiscounts)[0]
      const abIds = product.id + '|' + abDiscountId
      const discountIds = abDiscountId.split('|')

      // Check if all required products are present
      if (discountIds.some((id) => !this.productCounts[id])) {
        return { [abIds]: 0 }
      }

      // Calculate discount values for each item
      let discountValue = 0
      Object.keys(abDiscounts).forEach(ids => {
        let discountPrice = 0
        let maxCount = Infinity
        ids.split('|').forEach(id => {
          const productCount = this.productCounts[id]
          if (productCount && maxCount > productCount) maxCount = productCount
        })
        const regularPricesSum = ids.split('|').reduce((sum, id) => {
          sum += products[id].price
          return sum
        }, product.price) * maxCount

        discountPrice = maxCount * abDiscounts[ids]
        discountValue = regularPricesSum - discountPrice
      })

      return ({ [abIds]: discountValue })
    }).filter(discount => !!Object.values(discount)[0]) // filter 0-discounts

    // Calculate total ab discount
    const totalDiscount = this.abDiscounts.reduce((sum, discount) =>
      sum + Object.values(discount)[0], 0)

    return totalDiscount
  }
}

module.exports = Checkout
