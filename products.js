const Product = require('./product')

// Available products in DB
const products = {
  banana: new Product('banana', 3, { 3: 8, 5: 9 }, { 'apple|orange': 4 }),
  apple: new Product('apple', 1, { 4: 3 }),
  orange: new Product('orange', 2)
}

module.exports = products
