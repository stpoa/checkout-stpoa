const test = require('tape')
const Checkout = require('./')
const products = require('./products')

test('testing environment works', (t) => {
  t.equal(typeof Date.now, 'function')
  t.end()
})

test('creates checkout object', (t) => {
  const checkout = new Checkout()
  t.equal(typeof checkout, 'object')
  t.equal(
    typeof checkout.add,
    typeof checkout.receipt,
    typeof checkout.sum,
    'function'
  )
  t.end()
})

test('ads products to checkout', (t) => {
  const checkout = new Checkout()
  checkout.add(products.banana)
  checkout.add(products.banana, 2)
  checkout.add(products.apple)
  t.equal(checkout.productCounts[products.banana.id], 3)
  t.equal(checkout.productCounts[products.apple.id], 1)
  t.end()
})

test('sums products without discounts', (t) => {
  const { apple, banana } = products
  const checkout = new Checkout()
  checkout.add(banana)
  checkout.add(apple)
  t.equal(checkout.sum(), banana.price + apple.price)
  t.end()
})

test('sums products with quantity discount', (t) => {
  const checkout = new Checkout()
  checkout.add(products.banana, 10)
  const sum = checkout.sum()
  t.equal(sum, 18)
  t.equal(checkout.quantityDiscounts[0]['banana'], 12)
  t.end()
})

test('sums products with AB discount', (t) => {
  const checkout = new Checkout()
  const { apple, banana, orange } = products
  checkout.add(apple, 2)
  checkout.add(banana, 2)
  checkout.add(orange, 3)
  const sum = checkout.sum()
  t.equal(sum, 10)
  t.equal(checkout.abDiscounts[0]['banana|apple|orange'], 4)
  t.end()
})

test('sums products with percent discount', (t) => {
  const checkout = new Checkout()
  const { orange } = products
  checkout.add(orange, 300)
  const sum = checkout.sum()
  t.equal(sum, 540)
  t.equal(checkout.percentDiscount, 60)
  t.end()
})

test('generates proper receipt', (t) => {
  const checkout = new Checkout()
  const { apple, banana, orange } = products
  checkout.add(apple, 2)
  checkout.add(banana, 2)
  checkout.add(orange, 3)
  checkout.sum()
  const receipt = 'apple\tx2\t1 PLN\nbanana\tx2\t3 PLN\norange\tx3\t2 PLN\nDiscounts \nbanana+apple+orange\t\t-4 PLN\n'
  t.equal(checkout.receipt(), receipt)
  t.end()
})
