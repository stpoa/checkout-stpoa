# checkout #
## Simple checkout module with 3 discount types ##

### Installation ###

* Clone / Download
```git clone https://bitbucket.org/stpoa/checkout-stpoa```
* cd & install
```cd checkout-stpoa; npm install```

### Testing ###

* Linting
```npm run lint```

* Running tests
```npm run test```
