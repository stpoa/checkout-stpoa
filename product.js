/*
const product = {
  id: 'dfasdf',
  price: 10,
  quantityDiscounts: { 2: 102 }, // quantity: discountPrice
  abDiscounts: {
    'dfadsdrfa5': 23 // merged productsid, discountprice
  }
}
*/

// Product class
function Product (id, price, quantityDiscounts, abDiscounts) {
  this.id = id
  this.price = price
  this.quantityDiscounts = quantityDiscounts
  this.abDiscounts = abDiscounts
}

module.exports = Product
